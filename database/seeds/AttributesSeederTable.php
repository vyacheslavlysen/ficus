<?php

use Illuminate\Database\Seeder;

class AttributesSeederTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        factory(\App\Models\Attribute::class, 100)->create();
    }
}
