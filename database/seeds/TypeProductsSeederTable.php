<?php

use Illuminate\Database\Seeder;

class TypeProductsSeederTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        factory(\App\Models\TypeProduct::class, 100)->create();
    }
}
