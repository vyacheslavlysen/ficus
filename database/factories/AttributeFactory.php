<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Attribute;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

$types = [
    Attribute::TYPE_INT,
    Attribute::TYPE_FLOAT,
    Attribute::TYPE_STRING
];
$factory->define(Attribute::class, function (Faker $faker) use ($types) {
    $type = $types[rand(0, count($types) - 1)];
    $values = null;
    if ($type === Attribute::TYPE_STRING && rand(1, 2) % 2) {
        for ($i = 0; $i < rand(0, 20); $i++) {
            $values[] = $faker->slug(3);
        }

    }

    return [
        'name' => $faker->slug(2),
        'type' => $type,
        'values' => $values
    ];
});
