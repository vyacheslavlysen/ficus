<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\TypeProduct;
use Illuminate\Support\Str;
use Faker\Generator as Faker;


$factory->define(TypeProduct::class, function (Faker $faker) {

    return [
        'name' => $faker->slug(2),
    ];
});
