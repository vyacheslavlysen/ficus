# TEST Ficus

### Installation

1. ```git clone https://github.com/Laradock/laradock.git```
2. copy .env, .env(./laradock), docker-compose.yml(./laradock)
3. ```make start```
4. ```make cmd```
5. ```composer install``` 
6. ```php artisan key:generate```
7. ```php artisan migrate```
8. ```php artisan db:seed```
