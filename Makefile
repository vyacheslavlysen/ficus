start:
	cd laradock && docker-compose up -d mysql nginx redis
stop:
	cd laradock && docker-compose down
cmd:
	cd laradock && docker-compose exec --user=laradock workspace bash
mysql:
	cd laradock && docker-compose exec mysql bash
a:
	php artisan
